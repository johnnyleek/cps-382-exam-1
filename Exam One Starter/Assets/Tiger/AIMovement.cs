﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIMovement : MonoBehaviour
{
    [SerializeField]
    private float delay;

    private float nextMovement = 0.0F;
    private NavMeshAgent navMeshAgent;
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckNextMovement();
        ControlAnimations();
    }

    private void CheckNextMovement()
    {
        if (Time.time > nextMovement)
        {
            nextMovement = Time.time + delay;
            navMeshAgent.SetDestination(RandomNavmeshLocation(10f));
        }
    }

    private void ControlAnimations()
    {
        if (ObjectIsMoving())
        {
            animator.SetTrigger("Moving");
        }
       
    }

    private bool ObjectIsMoving()
    {
        return navMeshAgent.velocity.magnitude / navMeshAgent.speed > 0.1f;
    }

    private Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
}
