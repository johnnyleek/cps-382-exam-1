﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MazeScript : MonoBehaviour
{

    public Text winText;
    
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
            StartCoroutine(PlayWinText());
    }

    private IEnumerator PlayWinText()
    {
        winText.text = "You win! Going back to main menu...";
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("Scene One");
    }

}
