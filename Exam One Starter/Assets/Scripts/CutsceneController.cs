﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneController : MonoBehaviour
{
    public GameObject[] cameras;

    public void Start()
    {
        foreach(GameObject camera in cameras)
        {
            camera.SetActive(false);
        }

        StartCoroutine(Cutscene());
    }

    private IEnumerator Cutscene()
    {
        cameras[0].SetActive(true);
        yield return new WaitForSeconds(7f);
        cameras[0].SetActive(false);
        cameras[1].SetActive(true);
        yield return new WaitForSeconds(5f);
        cameras[1].SetActive(false);
        cameras[2].SetActive(true);
    }
}
