﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTwoController : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Scene One");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
            SceneManager.LoadScene("Scene Three");
    }
}
