﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Basic Movement Properties")]
    public float walkSpeed;
    public float runSpeed;
    public float gravity;
    public float jumpHeight;

    [Header("Ground Check Properties")]
    public Transform groundCheck;
    public float groundDistance;
    public LayerMask groudMask;

    private CharacterController controller;
    private bool isGrounded;
    private Vector3 verticalVelocity;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckHorizontalMovement();
        CheckVerticalMovement();
    }

    private void CheckHorizontalMovement()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        Vector3 movement = transform.right * x + transform.forward * z;
        if (Input.GetKey(KeyCode.LeftShift))
            controller.Move(movement * runSpeed * Time.deltaTime);
        else
            controller.Move(movement * walkSpeed * Time.deltaTime);
    }

    private void CheckVerticalMovement()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groudMask);
        if (verticalVelocity.y < 0f)
            verticalVelocity.y = -2f;
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
            verticalVelocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        verticalVelocity.y += gravity * Time.deltaTime;
        controller.Move(verticalVelocity * Time.deltaTime);
    }
}
